#!/bin/bash
#
# Program: To install vim, git, vundle. NERDTree and dress up .vimrc automatically.
# 
# Establish: 2019/07/16
# Creator: Queenie Lee

apt update
apt upgrade

#Install vim
apt install vim -y

#Install git
apt install git -y

#Install vundle
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

#Copy .vimrc for vundle and Plugins installation
cp ./.vimrc ~/.vimrc

#Installation
vim +PluginInstall +qall

#Install fonts
mkdir -p ~/.local/share/fonts
cp *ttf ~/.local/share/fonts
fc-cache -f -v

#Install tweak-tool
add-apt-repository universe
apt install gnome-tweak-tool -y
